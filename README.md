# SNTSolution-NodejsTest

***

## 1/ Database schema design

Sau khi tìm hiểu những yêu cầu dựa trên tình huống được cho, em đã list ra danh sách các entities sẽ có trong database schema:

- Company
- Department
- Staff
- Role
- Product
- Discount
- Order
- Customer (Dùng để lưu thông tin thanh toán, có thể sử dụng cho CRM sau này)

Trong đó mối quan hệ giữa các entities như sau:

- Company (1:n) Department
- Department (1:n) Staff
- Staff (1:1) Role
- Company (1:n) Product
- Product (n:n) Discount
- Product (n:n) Order
- Customer (1:n) Order

Để giải quyết các mối quan hệ many-to-many, chúng ta sẽ có thêm 2 entities là:

- DiscountApplyFor (trung gian giữa Product và Discount)
- OrderLine (trung gian giữa Product và Order)

Vậy chúng ta sẽ có 10 entities khi thiết kế database này. Để dễ hình dung hơn, em sẽ sử dụng Draw.io để vẽ Entity-Relation Diagram (ERD) và được hiển thị như bên dưới:

![ERD for this scenario](https://gitlab.com/quoctai101/sntsolution-nodejstest/-/raw/main/Images/ERD.jpg)

***

## 2/ Algorithm

### Algorithm 1

Anh/chị có thể xem nội dung code của em tại [Algorithm/Algorithm1.js](https://gitlab.com/quoctai101/sntsolution-nodejstest/-/blob/main/Algorithm/Algorithm1.js)

<p>Ngoài ra em có tạo một CodeSandbox để tiện cho việc testing của anh chị tại [đây](https://codesandbox.io/p/sandbox/algorithm1-flvvni?file=%2Findex.js): [https://codesandbox.io/p/sandbox/algorithm1-flvvni?file=%2Findex.js](https://codesandbox.io/p/sandbox/algorithm1-flvvni?file=%2Findex.js)<br>
<em>Lưu ý: Cần phải đăng nhập bằng GitHub hoặc Email và Fork về để chạy thử</em></p>

### Algorithm 2

Vì thời gian có hạn nên em đã sử dụng Đệ quy (Recursion) để giải quyết vấn đề này. Việc sử dụng đệ quy tốn lượng lớn tài nguyên khi gặp input lớn như 100 hay 200 nên giải pháp của em chỉ giải quyết được input lớn nhất là 32 khi chạy trên CodeSandbox, đây cũng chính là điểm chưa tốt trong giải pháp của em

Anh/chị có thể xem nội dung code của em tại [Algorithm/Algorithm2.js](https://gitlab.com/quoctai101/sntsolution-nodejstest/-/blob/main/Algorithm/Algorithm2.js)

<p>Ngoài ra em có tạo một CodeSandbox để tiện cho việc testing của anh chị tại [đây](https://codesandbox.io/p/sandbox/algorithm2-he4u4x?file=%2Findex.js): [https://codesandbox.io/p/sandbox/algorithm2-he4u4x?file=%2Findex.js](https://codesandbox.io/p/sandbox/algorithm2-he4u4x?file=%2Findex.js)<br>
<em>Lưu ý: Cần phải đăng nhập bằng GitHub hoặc Email và Fork về để chạy thử</em></p>
