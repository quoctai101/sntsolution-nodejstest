// Algorithm 2

let result = [];
let arraySession = [];

let retrieveSession = (remainTime) => {
  if (!remainTime) {
    tempArray = arraySession.slice();
    result.push(tempArray);
  } else {
    for (let sessionTime = 1; sessionTime <= 2; sessionTime++) {
      if (sessionTime <= remainTime) {
        arraySession.push(sessionTime);
        remainTime -= sessionTime;
        retrieveSession(remainTime);
        remainTime += arraySession.pop();
      }
    }
  }
};

let hourFill = 7;

retrieveSession(hourFill);

console.log("To fill " + hourFill + " hours:");

result.forEach((sessionSchedule) => {
  console.log(sessionSchedule);
});