// Algorithm 1

console.log("Node.js Algorithm 1");

let defaultMax = 3;

let f = () => {
  let d = new Date(); // current time
  return d.getMilliseconds() % 2 == 0; // => true or false
};

let retry = (callback, wait = 0, options = { max: 3 }) => {
  // in case options object was passed into function but max value was not defined correctly
  if (options.max === undefined) {
    options.max = defaultMax;
  } else {
    options.max = parseInt(options.max);
    options.max = isNaN(options.max) ? defaultMax : options.max;
  }

  if (options.max && callback() !== true) {
    console.log("Attempt failed! Retrying...");
    options.max--;
    setTimeout(retry.bind(this, callback, wait, options), wait);
  }
};

retry(f, 1000, { max: 3 });